# NodeJS ConsoleApp
A simple console application giving the user 3 options:
1. "Read package.json"  => Print out the contents of package.json.
2. "Display OS info"    => Print out miscellaneous info like system memory, CPU cores, platform and current user.
3. "Start HTTP Server"  => Start a local HTTP server on port 3000.

Inputting something invalid (e.g. "4" or "test") will result in an error message.

