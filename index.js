const { fstat } = require('fs');
const os = require('os');
const http = require('http');

const readline = require('readline').createInterface({
	input: process.stdin,
	output: process.stdout,
});
const fs = require('fs');

const text = `\nChoose an option:
1. Read package.json
2. Display OS info
3. Start HTTP server
Type a number: `;

const readFile = () => {
	fs.readFile('package.json', 'utf8', function (err, data) {
		console.log('\n');

		if (err) {
			return console.log(err);
		}
		console.log(data);
	});
};

const osInfo = () => {
	console.log(`\nGetting OS info...
    SYSTEM MEMORY: ${(os.totalmem() / 1024 / 1024 / 1024).toFixed(2)} GB
    FREE MEMORY: ${(os.freemem() / 1024 / 1024 / 1024).toFixed(2)} GB
    CPU CORES: ${os.cpus().length}
    ARCH: ${os.arch()}
    PLATFORM: ${os.platform()}
    RELEASE: ${os.release()}
    USER: ${os.userInfo().username}`);
};

const startServer = () => {
	console.log('Starting HTTP server...');

	const server = http.createServer((req, res) => {
		if (req.url === '/') {
			res.write('Hello World');
			res.end();
		}
	});

	server.listen(3000);
	console.log('listening on port 3000...');
};

readline.question(text, (option) => {
	switch (option) {
		case '1':
			readFile();
			break;
		case '2':
			osInfo();
			break;
		case '3':
			startServer();
			break;
		default:
			console.log('Invalid option');
	}
	readline.close();
});
